using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.ObjectPool;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Templates_.netcore.Helpers;
using Templates_.netcore.Interfaces;
using Templates_.netcore.Services;

namespace Templates_.netcore
{
    public class Startup
    {
        private string _product = "Templates Services";
        private string _version = "1.0.0.0";
        private string _document = "Templates .netcore";
        private string _route_prefix = String.Empty;
        private string[] _with_origins = { };

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Version
            try
            {
                Assembly assemblys = typeof(Startup).GetTypeInfo().Assembly;
                _product = assemblys.GetCustomAttribute<AssemblyProductAttribute>().Product;
                _version = assemblys.GetCustomAttribute<AssemblyFileVersionAttribute>().Version;
                _document = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                _route_prefix = Configuration.GetSection("AppSettings").GetValue<String>("RoutePrefix");
                _with_origins = Configuration.GetSection("AppSettings").GetSection("WithOrigins").Get<String[]>();
            }
            catch
            {
                _product = "Templates Services";
                _version = "1.0.0.0";
                _document = "Templates .netcore.xml";
                _route_prefix = String.Empty;
            }
            #endregion

            services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .WithOrigins(_with_origins)
                       .AllowCredentials();
            }));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(_version, new OpenApiInfo { Title = _product, Version = _version });
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, _document));
            });
            
            services.AddControllers();

            services.AddLocalization(o => o.ResourcesPath = "Resources");

            #region Configure app & connection setting
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.Configure<ConnectionStrings>(Configuration.GetSection("ConnectionStrings"));
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[] { new CultureInfo("th-TH") };
                options.DefaultRequestCulture = new RequestCulture("th-TH", "th-TH");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });
            #endregion

            #region Dependency injection            
            services.AddSingleton<ObjectPoolProvider, DefaultObjectPoolProvider>();
            services.AddTransient<IUtilityService, UtilityService>();
            #endregion

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseCors("CorsPolicy");
            app.UseSwagger();
            app.UseRouting();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint(string.Format("{0}/swagger/{1}/swagger.json", _route_prefix, _version), string.Format("{0} {1}", _product, _version));
                c.InjectJavascript(string.Format("{0}/swagger-ui/custom.js", _route_prefix), "text/javascript");
                c.DocumentTitle = string.Format("{0} API", _product);
                c.RoutePrefix = "documents";

            });
            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);
            app.UseStaticFiles();
            app.UseHttpsRedirection();
            //app.UseAuthentication();
            //app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
