﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Templates_.netcore.Entities.Embed
{
    /// <summary>
    /// การเข้าถึงของผู้ใช้
    /// </summary>
    [BsonIgnoreExtraElements]
    public class embed_user_rule
    {
        /// <summary>
        /// รหัสกลุ่มผู้ใช้
        /// </summary>
        public Int64 user_type { get; set; }
        /// <summary>
        /// ชื่อกลุ่มผู้ใช้
        /// </summary>
        public string user_type_name { get; set; }
        /// <summary>
        /// รหัสรูปภาพผู้ใช้งาน
        /// </summary>
        public Int64 content_id { get; set; }
        /// <summary>
        /// รูปภาพผู้ใช้งาน
        /// </summary>
        public string content_path { get; set; }
    }
}
