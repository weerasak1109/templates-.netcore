﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Templates_.netcore.Entities.MongoDB
{

    /// <summary>
    /// ข้อมูลพื้นฐาน
    /// </summary>
    public class BaseEntity
    {
        /// <summary>
        /// รหัส
        /// </summary>
        public ObjectId _id { get; set; }
    }

    /// <summary>
    /// ข้อมูลคงที่
    /// </summary>
    public abstract class BaseConstEntity : BaseEntity
    {
        /// <summary>
        /// ไม่ใช้งาน
        /// </summary>
        public Boolean inactive { get; set; }
        /// <summary>
        /// ผู้สร้าง
        /// </summary>
        public Int64 create_by { get; set; }
        /// <summary>
        /// เวลาสร้าง
        /// </summary>
        public DateTime create_date { get; set; }
        /// <summary>
        /// สร้าง
        /// </summary>
        /// <param name="user_id">ผุ้สร้าง</param>
        public virtual void Create(Int64 user_id)
        {
            this.create_by = user_id;
            this.create_date = DateTime.Now;
            this.inactive = false;
        }
    }

    /// <summary>
    /// ข้อมูลตั้งค่า
    /// </summary>
    public abstract class BaseSettingEntity : BaseConstEntity
    {
        /// <summary>
        /// ผู้แก้ไข
        /// </summary>
        public Int64 edit_by { get; set; }
        /// <summary>
        /// เวลาแก้ไข
        /// </summary>
        public DateTime edit_date { get; set; }
        /// <summary>
        /// แก้ไข
        /// </summary>
        /// <param name="user_id">ผู้แก้ไข</param>
        public virtual void Edit(Int64 user_id)
        {
            this.edit_by = user_id;
            this.edit_date = DateTime.Now;
        }
    }

    /// <summary>
    /// ข้อมูลหลัก
    /// </summary>
    public abstract class BaseMasterEntity : BaseSettingEntity
    {
        /// <summary>
        /// รหัสสปอนเซอร์
        /// </summary>
        public Int64 wsp_id { get; set; }
    }

    /// <summary>
    /// ข้อมูลรายการ
    /// </summary>
    public abstract class BaseTransactionEntity : BaseMasterEntity
    {
    }

}
