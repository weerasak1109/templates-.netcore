﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Templates_.netcore.Entities.Embed;

namespace Templates_.netcore.Entities.MongoDB
{
    /// <summary>
    /// การเข้าถึงของผู้ใช้
    /// </summary>
    [BsonIgnoreExtraElements]
    public class m_user_rule : BaseMasterEntity
    {
        /// <summary>
        /// รหัสประเภทผู้ใช้
        /// </summary>
        public Int64 user_type_id { get; set; }
        /// <summary>
        /// รหัสประเภทผู้ใช้
        /// </summary>
        public Int64 user_type { get; set; }
        /// <summary>
        /// ชื่อกลุ่มผู้ใช้
        /// </summary>
        public string user_type_name { get; set; }
        /// <summary>
        /// สังเกต
        /// </summary>
        public string remark { get; set; }
        /// <summary>
        /// rule
        /// </summary>
        public List<embed_user_rule> rule { get; set; }
    }
}
