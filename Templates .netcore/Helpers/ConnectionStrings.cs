﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Templates_.netcore.Helpers
{
    /// <summary>
    /// Connection strings
    /// </summary>
    public class ConnectionStrings
    {
        /// <summary>
        /// Mssql context
        /// </summary>
        public string MsSqlContext { get; set; }
        /// <summary>
        /// Nosql context
        /// </summary>
        public string NoSqlContext { get; set; }
    }
}
