﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Templates_.netcore.Entities.MongoDB;
using Templates_.netcore.Models;

namespace Templates_.netcore.Interfaces
{
    /// <summary>
    /// 
    /// </summary>
    public interface IUtilityService
    {
        public OutputModels<m_user_rule> TestLogin(Int64 wsp_id);
    }
}
