﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Templates_.netcore.Models;

namespace Templates_.netcore.Utilities
{
    /// <summary>
    /// Error utility
    /// </summary>
    public class ErrorUtility<TValue>
    {
        /// <summary>
        /// สำเร็จ
        /// </summary>
        public const int SUCCESS = 1;

        /// <summary>
        /// Get success message
        /// </summary>
        /// <returns></returns>
        public static OutputModels<TValue> GetSuccessMessage(TValue data)
        {
            var output = new OutputModels<TValue>()
            {
                data = data,
                error_code = SUCCESS,
                sub_code = SUCCESS,
                message = "สำเร็จ",
                title = "แจ้งเตือน"
            };
            return output;
        }

        /// <summary>
        /// Get error message
        /// </summary>
        /// <returns></returns>
        public static OutputModels<TValue> GetErrorMessage(string error_code, string message)
        {
            try
            {
                string[] err = error_code.Split("-");
                var output = new OutputModels<TValue>();
                if (err.Length == 2)
                {
                    output.error_code = Convert.ToInt64(err[0]);
                    output.sub_code = Convert.ToInt64(err[1]);
                    output.title = "ข้อผิดพลาด";
                    output.message = message;
                }
                else
                {
                    output.error_code = 6784;
                    output.sub_code = 1;
                    output.title = "ข้อผิดพลาด";
                    output.message = message;
                }
                return output;
            }
            catch (Exception ex)
            {
                string[] err = INTERNAL.Split("-");
                var output = new OutputModels<TValue>();
                if (err.Length == 2)
                {
                    output.error_code = Convert.ToInt64(err[0]);
                    output.sub_code = Convert.ToInt64(err[1]);
                    output.title = "ข้อผิดพลาด";
                    output.message = ex.Message;
                }
                else
                {
                    output.error_code = 6784;
                    output.sub_code = 1;
                    output.title = "ข้อผิดพลาด";
                    output.message = ex.Message;
                }
                return output;
            }
        }

        /// <summary>
        /// ข้อผิดพลาดภายใน
        /// </summary>
        public const string INTERNAL = "6784-1";
    }
}
