﻿using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Templates_.netcore.Entities.MongoDB;
using Templates_.netcore.Helpers;
using Templates_.netcore.Interfaces;
using Templates_.netcore.Models;
using Templates_.netcore.Resources;
using Templates_.netcore.Utilities;

namespace Templates_.netcore.Services
{
    public class UtilityService : IUtilityService
    {
        private readonly ConnectionStrings _connect;
        private readonly IStringLocalizer _localizer;
        private readonly ILogger _logger;
        private readonly IMongoCollection<m_user_rule> m_userrule;

        /// <summary>
        /// Utility service constructor
        /// </summary>
        /// <param name="logger">logger</param>
        /// <param name="connect">connect</param>
        /// <param name="factory">factory</param>
        public UtilityService(ILogger<UtilityService> logger,
            IOptions<ConnectionStrings> connect,
            IStringLocalizerFactory factory)
        {
            try
            {
                _logger = logger;
                _connect = connect.Value;

                var type = typeof(ErrorMessage);
                var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
                _localizer = factory.Create(typeof(ErrorMessage).Name, assemblyName.Name);

                var client = new MongoClient(_connect.NoSqlContext);
                var database = client.GetDatabase(new MongoUrl(_connect.NoSqlContext).DatabaseName);
                m_userrule = database.GetCollection<m_user_rule>(typeof(m_user_rule).Name);


            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format("UtilityService -- {0}", ex.Message));
            }
        }


        public OutputModels<m_user_rule> TestLogin(Int64 wsp_id)
        {
            try
            {
               List<m_user_rule> models = m_userrule.Find(i => i.inactive == false).ToList();
                return ErrorUtility<m_user_rule>.GetSuccessMessage(models[0]);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format("TestLogin -- {0}", ex.Message));
                string error_code = ErrorUtility<m_user_rule>.INTERNAL;
                return ErrorUtility<m_user_rule>.GetErrorMessage(error_code, string.Format("{0}--{1}", _localizer[error_code].Value, ex.Message));
            }
        }

    }
}
