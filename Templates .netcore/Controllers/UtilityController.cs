﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Templates_.netcore.Entities.MongoDB;
using Templates_.netcore.Interfaces;
using Templates_.netcore.Models;

namespace Templates_.netcore.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Route("api/utility")]
    [ApiController]
    public class UtilityController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IUtilityService _utility;

        public UtilityController(ILogger<UtilityController> logger, IUtilityService utility)
        {
            try
            {
                _logger = logger;
                _utility = utility;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format("UtilityController -- {0}", ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("test")]
        public ActionResult<OutputModels<m_user_rule>> TestLogin([FromQuery]Int64 wsp_id)
        {
            try
            {
                OutputModels<m_user_rule> login = _utility.TestLogin(wsp_id);
                return Ok(login);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, string.Format("TestLogin -- {0}", ex.Message));
                 return StatusCode(500, ex);
            }
        }

    }
}
